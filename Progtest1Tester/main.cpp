#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <time.h>
using namespace std;

int main () {
    system(R"(del C:\Users\zilin\CLionProjects\AG1\Progtest1Tester\cmake-build-debug\example.txt)");
    vector <pair<int, int>> edges;
    set<int> x;

    srand(time(0));
    for(int i = 0 ; i <= 1000 ; i++){
        int c1 = rand()%15 , c2 = rand()%15;
        if (c1 != c2)
            edges.emplace_back(make_pair( c1 , c2 ));
    }
    ofstream myfile;
    myfile.open ("example.txt");
    myfile << "15 " << edges.size() + 1 << endl;
    myfile << "0 14 0" << endl << "0\n0" << endl;
    for (auto & edge : edges) {
        if ((edge.first == 0 && edge.second == 14))
            cout << "Invalid point" << endl;
        else if ((edge.first == 14 && edge.second == 0))
            cout << "Ivanlid point" << endl;
        else
            myfile << edge.first << " " << edge.second << endl;
    }
    myfile.close();
    return 0;
}