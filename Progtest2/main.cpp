#ifndef __PROGTEST__
#include <chrono>
#include <ctime>
#include "flib.h"
#endif //__PROGTEST__

void swap(int *x, int *y){
    int tmp = *x;
    *x = *y;
    *y = tmp;
}
/**
 * @param buff target to load in
 * @param in_file index of file we are reading from
 * @param max number of ints to load
 * @param done sets bool to true if buffer is empty
 * @returns -1 on 0 ints loaded or returns number of ints loaded
 * */
void LoadData (int in_file , int * buff , int max , bool & done , int & size, bool & Need, int & idx){
    int tmp= flib_read(in_file, buff , max);
    if (!tmp)
        done = true;
    else
        done = false;
    size = tmp;
    Need = false;
    idx = 0;
}
void printFile(int number){
    flib_open(number, READ);
    int * tmp = new int[10];
    printf("\n\tPrinting file #%d:\n", number);
    while(true){
        int size = flib_read(number, tmp, 10 );
        if (size == 0)
            break;
        for (int i = 0 ; i < size ; i++)
            printf("%d ", tmp[i]);
        printf("\n");
    }
    delete[] tmp;
    flib_close(number);
}

/** Inspired at https://www.geeksforgeeks.org/cpp-program-for-heap-sort/ */
void HeapIf(int * array, int n, int root){
    int largest = root;
    int left = 2 * root + 1;
    int right = 2 * root + 2;
    if (left < n && array[left] > array[largest])
        largest = left;
    if (right < n && array[right] > array[largest])
        largest = right;
    if (largest != root){
        swap(&array[root], &array[largest]);
        HeapIf(array, n, largest);
    }
}
void heapSort(int * array, int n){
    for (int i = n / 2 - 1; i >= 0; i--)
        HeapIf(array, n, i);
    for (int i = n - 1; i >= 0; i--){
        swap(&array[0], &array[i]);
        HeapIf(array, i, 0);
    }
}

void Merger(bool &NeedB1, bool &NeedB2, bool &file1_done, bool &file2_done,
            int  &size1, int  &size2, int  &idx1, int  &idx2,int & idxRes,
            int * buff1 , int * buff2 , int * result, int out_file){
    if (file1_done){
        flib_write(out_file , &buff2[idx2] , size2 - idx2);
        NeedB2 = true;
        return;
    }
    else if (file2_done){
        flib_write(out_file , &buff1[idx1] , size1 - idx1);
        NeedB1 = true;
        return;
    }
    while(true){
        if (idx1 == size1){
            NeedB1 = true;
            break;
        }
        else if (idx2 == size2){
            NeedB2 = true;
            break;
        }
        else if (buff1[idx1] <= buff2[idx2]){
            result[idxRes] = buff1[idx1];
            idxRes++;
            idx1 ++;
        }
        else if (buff2[idx2] < buff1[idx1]){
            result[idxRes] = buff2[idx2];
            idxRes++;
            idx2 ++;
        }
    }
    flib_write(out_file , result , idxRes);
    idxRes = 0;
}
void FileMerger(int in_file1 , int in_file2 , int out_file ,int bytes){
    flib_open(in_file1,READ);
    flib_open(in_file2,READ);
    flib_open(out_file,WRITE);

    int max = bytes / sizeof(int);

    int * buff1     = (int*)malloc(max/4 * sizeof(int));
    int * buff2     = (int*)malloc(max/4 * sizeof(int));
    int * result    = (int*)malloc(max/2 * sizeof(int));

    bool  NeedB1        = true;
    bool  NeedB2        = true;
    bool  file1_done    = false;
    bool  file2_done    = false;
    int   size1         =   0;
    int   size2         =   0;
    int   idx1          =   0;
    int   idx2          =   0;
    int   idxRes        =   0;

    while(true){ // Loop 1
        if (NeedB1) {
//            printf("Buffer1 refresh: ");
            LoadData(in_file1, buff1, max / 4, file1_done, size1, NeedB1, idx1);
        }
        if (NeedB2) {
//            printf("Buffer2 refresh: ");
            LoadData(in_file2, buff2, max / 4, file2_done, size2, NeedB2, idx2);
        }
        if (file2_done && file1_done)
            break;
        else{
            Merger(NeedB1, NeedB2, file1_done, file2_done, size1,
                   size2, idx1, idx2, idxRes,buff1 , buff2 , result, out_file);
        }
    }
    free(buff1);
    free(buff2);
    free(result);
    flib_close(in_file1);
    flib_close(in_file2);
    flib_close(out_file);
    flib_remove(in_file1);
    flib_remove(in_file2);
}
void tarant_allegra ( int32_t in_file, int32_t out_file, int32_t bytes ){
    flib_open(in_file, READ);
    int * buffer = new int[(bytes/sizeof(int))];

    int NumberCounter = 2;
    for (int bufferLenght = 0; ; NumberCounter++){
        bufferLenght = flib_read(in_file, buffer , (bytes / 4));
        if (bufferLenght == 0)
            break;
        heapSort(buffer, bufferLenght);
        flib_open(NumberCounter  , WRITE);
        flib_write(NumberCounter , buffer , bufferLenght);
        flib_close(NumberCounter);
    }
//    printf("\n\n===================================================\n\n");
    flib_close(in_file);
    if (NumberCounter == 3){
        flib_open(NumberCounter - 1 , READ);
        int size = flib_read(NumberCounter - 1 , buffer , (bytes/sizeof(int)));
        heapSort(buffer , size);
        flib_open(out_file , WRITE);
        flib_write(out_file , buffer , size);
        flib_close(out_file);
        flib_close(NumberCounter - 1);
        delete[] buffer;
    }
    else {
        int start = 2;
        int WriteFile = NumberCounter;
        delete[] buffer;
        while(true){
            if (NumberCounter == 4){
//                printf("\t\tFinal#LastMerge %d + %d -> %d\n\n", start , start+1, out_file);
                FileMerger(start, start+1 , out_file , bytes);
                break;
            }
            if (WriteFile == 65000)
                WriteFile = 2;
            else if (start == 65000){
                start = 2;
                FileMerger(start, start+1 , WriteFile , bytes);
//                printf("\t1#Merge %d + %d -> %d\n", start , start+1, WriteFile);
                WriteFile ++;
                start+=2;
                NumberCounter --;
            }
            else{
//                printf("Merge %d + %d -> %d\n", start , start+1, WriteFile);
                FileMerger(start, start+1 , WriteFile , bytes);
                start+=2;
                NumberCounter --;
                WriteFile ++;
            }
        }
    }
}

#ifndef __PROGTEST__
uint64_t total_sum_mod;
void create_random(int output, int size){
    total_sum_mod=0;
    flib_open(output, WRITE);
    srand(time(NULL));
#define STEP 100ll
    int val[STEP];
    for(int i=0; i<size; i+=STEP){
        for(int j=0; j<STEP && i+j < size; ++j){
            val[j]=-1000 + (rand()%(2*1000+1));
            total_sum_mod += val[j];
        }
        flib_write(output, val, (STEP < size-i) ? STEP : size-i);
    }
    flib_close(output);
}
void check_result ( int out_file, int SIZE ){
    flib_open(out_file, READ);
    int q[30], loaded, last=-(1<<30), total=0;
    uint64_t current_sum_mod=0;
    while(loaded = flib_read(out_file, q, 30), loaded != 0){
        total += loaded;
        for(int i=0; i<loaded; ++i){
            if(last > q[i]){
                printf("the result file contains numbers %d and %d on position %d in the wrong order!\n", last, q[i], i-1);
                exit(1);
            }
            last=q[i];
            current_sum_mod += q[i];
        }
    }
    if(total != SIZE){
        printf("the output contains %d but the input had %d numbers\n", total, SIZE); exit(1);
    }
    if(current_sum_mod != total_sum_mod){
        printf("the output numbers are not the same as the input numbers\n");
        exit(1);
    }
    flib_close(out_file);
}

int main(int argc, char **argv){
    const uint16_t MAX_FILES = 65535;
    flib_init_files(MAX_FILES);
    int INPUT = 0;
    int RESULT = 1;
    int SIZE = 3000000;
    create_random(INPUT, SIZE);
    tarant_allegra(INPUT, RESULT , 2000);
    check_result(RESULT, SIZE);
    flib_free_files();
    system("rm ~/CLionProjects/AG1/Progtest2/cmake-build-debug/[0-9]*");
    return 0;
}
#endif //__PROGTEST__
