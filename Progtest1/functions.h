/*
 * @author Zilinek Jakub 
 * on 10.10.2020
 */
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <memory>
#pragma once
using namespace std;


struct Connection{
    int first;
    int second;
    Connection(int x, int y){
        this->first = x;
        this->second = y;
    }
    void Print() const{
        cout << first << " -> " << second << endl;
    }
};

void GetInput(int &top, int &edge,
              int & beginID, int & universityID, int & jumps,
              int & infPlan, set<int> & InfPlanets,
              int & expMed, set<int> & MedPlanets,
              vector<Connection> & Connections){
    cin >> top >> edge;
    cin >> beginID >> universityID >> jumps;

    cin >> infPlan;
    if (infPlan){
        int tmp;
        for (int i = 0 ; i < infPlan ; i++) {
            cin >> tmp;
            InfPlanets.insert(tmp);
        }
    }

    cin >> expMed;
    if (expMed){
        int tmp;
        for (int i = 0 ; i < expMed ; i++) {
            cin >> tmp;
            MedPlanets.insert(tmp);
        }
    }
    int first, second;
    for(int i = 0 ; i < edge ; i++){
        cin >> first >> second;
        if (first == -1)
            break;
        Connections.emplace_back(first,second);
    }
}

