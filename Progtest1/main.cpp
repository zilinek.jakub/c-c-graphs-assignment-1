#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <chrono>
using namespace std;

//---------------------------------PLANET--------------------------
class CPlanet{
public:
    int ID{};
    vector<int> children;
    bool Infected{};
    bool Medicine{};
    bool checked = false;

    CPlanet(int ID, bool Infected , bool Medicine ){
        this->Infected = Infected;
        this->Medicine = Medicine;
        this->ID = ID;
    }
    CPlanet() = default;
    void Print(){
        cout << "\t\tPlanet# " << ID << endl;
        cout << "Infected: " << Infected << " | Experimental Medicine: " << Medicine << endl;
        cout << children.size() << endl;
        cout << " #Friendly planet IDs: ";
        for (auto it : children)
            cout << it << " | ";
        cout << endl << endl;
    }
    friend bool operator<(const CPlanet& l, const CPlanet& r)
    {
        return l.ID > r.ID;
    }
};
//---------------------------------TREE----------------------------
class CTree{
public:
    int maxSteps{};
    int beginID{};
    int targetID = -1;


    map < int , CPlanet > Universe;

    //------RECURSION------
    void Find(){
        vector <int> stack;
        // Insert beginID into stack and set checked = true;
        stack.emplace_back(beginID);
        Universe.at(beginID).checked = true;
        int index;
        bool PlayerInfected = false;
        int stepsLeft = -1;
        int InfID = -1;
        vector<int> CheckedInf;
        vector<int> Pomocnicek;


        while(true){
            // sets index to last value
            index = stack.back();
            if (InfID == index) {
                CheckedInf.erase(CheckedInf.begin());
                for (auto it : CheckedInf)
                    Universe.at(it).checked = false;
                CheckedInf.clear();
                stepsLeft = maxSteps;
            }
            //Checking for end
            if( index == targetID || stack.empty() )
                break;

            if ( Universe.at(index).Infected && !PlayerInfected ) {
                InfID = index;
                PlayerInfected = true;
                stepsLeft = maxSteps;
            }
            if (PlayerInfected){
                if (stepsLeft <= 0) {
                    stack.pop_back();
                    index = stack.back();
                    stepsLeft++;
                    /*TODO Spatne pricitani kroku kdyz si vracime ze slepe ulicky*/
                    if (maxSteps == 0)
                        PlayerInfected = false;
                }
            }
            int sizeBefore = stack.size();
            for (auto it : Universe.at(index).children) {
                if (!Universe.at(it).checked) {
                    stack.emplace_back(it);
                    if (PlayerInfected) {
                        stepsLeft--;
                        CheckedInf.emplace_back(it);
                        Pomocnicek.emplace_back(it);
                    }
                    Universe.at(it).checked = true;
                    break;
                }
            }
            if ( sizeBefore == int(stack.size()) ) {
                if (InfID == index && PlayerInfected){
                    for (auto it : Pomocnicek)
                        Universe.at(it).checked = false;
                    Pomocnicek.clear();
                    InfID = -1;
                    PlayerInfected = false;
                }
                if (PlayerInfected) {
                    stepsLeft++;
                }
                stack.pop_back();
            }
        }
        if(stack.empty())
            stack.emplace_back(-1);
        for(auto it : stack)
            cout << it << " ";
    }
    CTree() = default;
    void GetInput(){
        int top , edge , infPlan , expMed;
        cin >> top >> edge;
        for (int i = 0 ; i < top ; i++)
            Universe.insert(make_pair(i, CPlanet(i, false, false))); // 10 000
        cin >> beginID >> targetID >> maxSteps;
        cin >> infPlan;
        if (infPlan){
            for (int i = 0 ; i < infPlan ; i++) {
                int tmp;
                cin >> tmp;
                Universe[tmp].Infected = true;
            }
        }
        cin >> expMed;
        if (expMed){
            for (int i = 0 ; i < expMed ; i++) {
                int tmp;
                cin >> tmp;
                Universe[tmp].Medicine = true;
            }
        }
        int first, second;
        for(int i = 0 ; i < edge ; i++){
            cin >> first >> second;
            Universe[first].children.emplace_back(second);
            Universe[second].children.emplace_back(first);
        }
    }
    void TreePrint(){
        for (auto it : Universe)
            it.second.Print();
    }
};
//---------------------------------MAIN---------------------------------
int main() {
    CTree tree;
    tree.GetInput();

//    auto start = std::chrono::high_resolution_clock::now();
    tree.Find();
//    auto end1 = std::chrono::high_resolution_clock::now();
//    std::chrono::duration<float> duration = end1 - start;
//    cout << "\n\nProcessing took: " << duration.count() << endl;
    return 0;
}